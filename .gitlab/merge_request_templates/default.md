# Checklist for this merge request ("MR")

## Contributing Checklist

🚨 Please review the [guidelines for contributing](CONTRIBUTING.md) to this repository.

- [ ] The MR originates from **a topic/feature/bugfix branch**. Do NOT open an MR from your `master` or `main`.

   `%{source_branch}` ... this SHOULD NOT be `master` or `main`.

- [ ] The MR subject or squashed commit message subject follows Conventional Commits.

- [ ] The work MUST not fail code style/linting or existing and new unit tests.

## Description

<!-- Whether one commit or many, i.e., squashing the work, use the below commit messages to describe the change. -->

%{all_commits} 


🤗 Thank you for your contribution!

<!-- note an open issue below by uncommenting the example -->
<!-- Fixes #123 -->

